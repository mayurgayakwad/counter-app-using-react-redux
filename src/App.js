import "./App.css";
import { incButton, decButton, resetButton } from "./action";
import { useSelector, useDispatch } from "react-redux";

function App() {
  const initialState = useSelector((state) => state.changeTheNumber);
  const dispatch = useDispatch();
  return (
    <>
      <center>
        <h1>Counter App</h1>
        <p>Using React and Redux</p>
      </center>
      <center>
        <h2>Counter : {initialState}</h2>{" "}
        <button className="btn-1" onClick={() => dispatch(incButton())}>
          Increment
        </button>
        <button className="btn-2" onClick={() => dispatch(decButton())}>
          Decrement
        </button>
        <button className="btn-3" onClick={() => dispatch(resetButton())}>
          Reset
        </button>
      </center>
    </>
  );
}

export default App;
