export const incButton = () => {
  return {
    type: "INCREMENT",
  };
};
export const decButton = () => {
  return {
    type: "DECREMENT",
  };
};
export const resetButton = () => {
  return {
    type: "RESET",
  };
};
